# D I M C H A

```
D I M C H A
e  \a  \\ n
s   g   \\g
k   e    \e
t         r
o
p
```
## What is this?
DImCha is a utility that sits around and swaps your background image every set
interval to a randomly chosen one.

## How do I use this?
Just run DImCha and follow the instructions :)

## Why use this over a crontab?
..Haven't figured out an answer to this one quite yet. DImCha is a happy
little guy though.

## Why D? Why not C++ HOW DARE YOU BE DIFFERENT!?
Because DImCha starts with a 'D', not a 'C++'. Used to be done in Python 2.7
though.

## Can I use N in my own project?
Go right ahead, just respect my license. Maybe even give me an honorable
mention in a comment (OPTIONAL)? :D

## Your tool ate my cat I hate you!
I'm pretty sure if DImCha eats your cat then you have bigger problems than
DImCha. I suggest aquiring the services of an exorcist, or failing this a
rather heavy hammer.

## (Random stream of expletives with no real coherent message)
The answer is yes. Except when it's no. I will need to consult myself for
this..

## Do I really need to call this thing DImCha?
No. I use linux. I therefore am unwilling to press shift except when I must.
DImCha's binary is simply 'dimcha', or 'dimcha.exe' on windows :)

I am really just refering to dimcha as DImCha to be obnoxious :P


