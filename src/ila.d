module dimcha.ila;

const string VERSION = "0.5.0";

/**
	The ILA format started as a simple gzipped plain-text file,
	but due to the discovery that it would be more memory-efficient
	to simply leave it as uncompressed, it is now just that.

	To decompress and parse the file, either the entire file,
	which could potentially have no upper size limit, would need to
	be loaded into ram, or it would need to be re-written to disk
	in a temporary file that is decompressed.

	The net disk savings are minimal compared to the memory costs,
	as the entire file's data is already stored in memory once.

	The first line is the 3-digit semantic version number.
	The second line is the number of images in the list.
	The remaining lines after this are all paths to images.
**/

import std.stdio: File;
import std.string: chomp;
import std.file: exists, isFile;
import std.conv: to, ConvException;
import std.random: randomSample, Random, unpredictableSeed;

import std.algorithm.searching: endsWith;

import dimcha.util;

class ILALoadError: Exception
{
	this(string path)
	{
		import std.string: format;
		super("ILA Load Error: Cannot load ILA file '%s'".format(path));
	}
}

class ILAComposureError: Exception
{
	this(string path)
	{
		import std.string: format;
		super("ILA Composure Error: Cannot create ILA from directory '%s'".format(path));
	}

	this(string path, string additionalInformation)
	{
		import std.string: format;
		super("ILA Composure Error: Cannot create ILA from directory '%s'\n%s".format(path, additionalInformation));
	}
}

@safe pure nothrow class ILAParseError: Exception
{
	this(string error)
	{
		super(error);
	}
}

@safe pure nothrow class ILASaveError: Exception
{
	this(string error)
	{
		super(error);
	}
}


struct ILA
{
	string versionSpec;
	size_t count;
	string[] list;

	/// Load an ILA file.
	void load(string source)
	{
		import core.exception: RangeError;
		if (!exists(source) || !isFile(source)) throw new ILALoadError(source);

		File fp = File(source, "r");

		try
		{
			versionSpec = fp.readln().chomp();
			count = to!size_t(fp.readln().chomp());

			string line;
			while ((line = fp.readln().chomp()) !is null)
				list ~= line;

			if (list.length != count)
			{
				import std.string: format;
				throw new ILAParseError("ILA Load Error: Malformed data in '%s'".format(source));
			}
		}
		
		catch (ConvException e)
		{
			import std.string: format;
			throw new ILAParseError("ILA Load Error: Malformed data in '%s'".format(source));
		}

		finally
		{
			fp.close();
		}
	}
	///
	unittest
	{
		ILA ila;
		ila.load("unittestFiles/dimcha/valid.ila");
		assert(ila.versionSpec == "0.5.0");
		assert(ila.count == 1);
		assert(ila.list[0] == "test.png");
	}

	unittest
	{
		ILA ila;
		try
		{
			ila.load("unittestFiles/dimcha/malformed.ila");
			assert(false);
		}
		catch (ILAParseError e)
			assert(true);
	}

	unittest
	{
		ILA ila;
		try
		{
			ila.load("unittestFiles/dimcha/arrayOutOfBounds.ila");
			assert(false);
		}
		catch (ILAParseError e)
			assert(true);
	}

	unittest
	{
		ILA ila;
		try
		{
			ila.load("unittestFiles/dimcha/non-existant.ila");
			assert(false);
		}
		catch (ILALoadError e)
			assert(true);
	}

	/// Save an ILA file.
	void save(string dest)
	{
		File fp = File(generatePath(dest, getDataPath()), "w");

		fp.writeln(VERSION);
		fp.writeln(count);
		foreach (path; list)
			fp.writeln(path);

		fp.close();
	}
	///
	unittest
	{
		ILA ila;

		ila.versionSpec = VERSION;
		ila.count = 1;
		ila.list ~= "testfile.png";

		ila.save("unittestFiles/dimcha/sample.ila");
	}

	/// Select a random item from the ILA image list.
	string select()
	{
		auto rhs = randomSample(this.list, 1, Random(unpredictableSeed)).front();
		return rhs;

	}
	///
	unittest
	{
		import std.stdio: writeln;

		ILA ila;

		ila.versionSpec = VERSION;
		ila.count = 5;
		ila.list = ["f1.png", "f2.png", "f3.png", "f4.png", "f5.png"];

		writeln(ila.select());
	}
}

/// Construct a new ILA from a given path
ILA composeToILA(string path)
{
	import std.file: dirEntries, SpanMode;
	import std.string: toLower;
	import std.path: extension;
	import std.algorithm.searching: canFind;

	if (!exists(path)) throw new ILAComposureError(path, "Cannot create ILA from non-existant path");
	if (isFile(path)) throw new ILAComposureError(path, "Cannot create ILA from an existing file");

	string[] flist;

	string[] acceptedExtensions =
	[
		".png",
		".bmp",
		".jpg",
		".jpeg"
	];

	foreach (string fname; dirEntries(path, SpanMode.breadth))
		if (acceptedExtensions.canFind(fname.toLower.extension))
			flist ~= fname;

	ILA rval;
	rval.versionSpec = VERSION;
	rval.count = flist.length;
	rval.list = flist;

	return rval;
}

