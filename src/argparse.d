module dimcha.argparse;

@safe pure nothrow class NotEnoughSubArgs: Exception
{
	this(string message)
	{
		super(message);
	}
}

struct argSet
{
	string sArg;
	string lArg;
	bool* flag;
	size_t numArgs;
	string[]* subArgs;
}

struct Opts
{
	argSet[] args;

	/// Register a new commandline arg to parse for.
	void register(string sArg, string lArg, bool* flag, size_t numArgs = 0, string[]* subArgs = null)
	{
		args ~= argSet(sArg, lArg, flag, numArgs, subArgs);
	}
}

/// Parse an array of strings(think the string[] args in your main function :P) against an Opts struct.
void parse(string[] args, Opts options)
{
	import std.algorithm.searching: canFind, find, countUntil;

	foreach(size_t i, ref arg; options.args)
	{
		if (args.canFind(arg.sArg))
		{
			// First just set the flag...
			if (options.args[i].flag !is null)
				*options.args[i].flag = true;

			// Now, test if we even need to scan for a list of sub args...
			if (options.args[i].numArgs > 0)
			{
				if (args.length >= args.countUntil(arg.sArg) + options.args[i].numArgs + 1)
				{
					for (size_t v = args.countUntil(arg.sArg) + 1; v < args.countUntil(arg.sArg) + options.args[i].numArgs + 1; v++)
						*options.args[i].subArgs ~= args[v];
				}
				else throw new NotEnoughSubArgs(arg.sArg);
			}
		} else
		if (args.canFind(arg.lArg))
		{
			// First just set the flag...
			if (options.args[i].flag !is null)
				*options.args[i].flag = true;

			// Now, test if we even need to scan for a list of sub args...
			if (options.args[i].numArgs > 0)
			{
				if (args.length >= args.countUntil(arg.lArg) + options.args[i].numArgs + 1)
				{
					for (size_t v = args.countUntil(arg.lArg) + 1; v < args.countUntil(arg.lArg) + options.args[i].numArgs + 1; v++)
						*options.args[i].subArgs ~= args[v];
				}
				else throw new NotEnoughSubArgs(arg.lArg);
			}
		}
	}
}

unittest
{
	Opts rawr;
	bool nya = false;

	string[] val_args;

	rawr.register("-n", "--nya", &nya);
	rawr.register("-v", "--val", null, 2, &val_args);

	string[] args = ["./dimcha", "-n", "-v", "rawr", "mod", "hi"];

	args.parse(rawr);

	assert(nya);
	assert(val_args.length == 2);
	assert(val_args == ["rawr", "mod"]);
}

