import std.stdio;
import dimcha.ila;
import dimcha.config;
import dimcha.util;
import dimcha.argparse;

import std.algorithm.searching: canFind, find, countUntil;
import std.string: format, split;

import std.process: execute;
import core.thread: Thread;

import std.file: exists, isFile;

int main(string[] args)
{
	bool breakLoop = false;
	string listName;
	ILA ila;
	ConfigFile cfg;

	if (firstRun())
	{
		writeln("Generating default configuration in %s.".format(getConfigPath()));
		cfg.loadDefault();
		cfg.save("%s/dimcha/settings.cfg".format(getConfigPath()));

		writeln("Please create an ILA file for dimcha to make selections from. See 'dimcha --help' for more information :)");
		writeln("You may also want to configure the default command used to display the image.");
		writeln("By default, dimcha will use 'feh --bg-max %s'");
		return 1;
	}

	try
		cfg.load("%s/dimcha/settings.cfg".format(getConfigPath()));
	catch (ConfigLoadError e)
	{
		writeln(e.msg);
		return 1;
	}

	catch (ConfigParseError e)
	{
		writeln(e.msg);
		return -1;
	}

	// These variables are used by the argparse system
	bool ARGHELP, ARGKILL, ARGLIST, ARGCOMMAND, ARGTIME, ARGCOMPOSE, ARGSILENT;
	string[] VALLIST, VALTIME, VALCOMPOSE;

	Opts options;
	options.register("-h", "--help", &ARGHELP);
	options.register("-k", "--kill", &ARGKILL);
	options.register("-l", "--list", &ARGLIST, 1, &VALLIST);
	options.register("-x", "--command", &ARGCOMMAND); // --command has to be handled in a special way
	options.register("-t", "--time", &ARGTIME, 1, &VALTIME);
	options.register("-c", "--compose", &ARGCOMPOSE, 2, &VALCOMPOSE);
	options.register("-s", "--silent", &ARGSILENT);

	try
	{
		args.parse(options);
	}
	catch (NotEnoughSubArgs e)
	{
		final switch (e.msg)
		{
			case "-l":
			case "--list":
				writeln("Dimcha: Missing list argument\nTry 'dimcha --help' for more information.");
				break;

			case "-x":
			case "--command":
				writeln("Dimcha: Missing command argument\nTry 'dimcha --help' for more information.");
				break;

			case "-t":
			case "--time":
				writeln("Dimcha: Missing time argument\nTry 'dimcha --help' for more information.");
				break;

			case "-c":
			case "--compose":
				writeln("Dimcha: Compose requires two arguments\nTry 'dimcha --help' for more information.");
				break;
		}
		return -1;
	}

	if (ARGHELP)
	{
		writeln(HELP);
		return 0;
	}

	if (ARGKILL)
	{
		breakLoop = true;
	}

	if (ARGLIST)
	{
		cfg.defaultList = generatePath(VALLIST[0], getDataPath());
		cfg.save("%s/dimcha/settings.cfg".format(getConfigPath()));
	}

	if (ARGCOMMAND)
	{
		import std.algorithm.searching: countUntil;
		string command;

		auto startIndex = args.countUntil("-x") + 1;
		if (startIndex < 0) startIndex = args.countUntil("--command");

		for (auto i = startIndex; i < args.length - 1; i++)
			command ~= args[i] ~= " ";

		command ~= args[args.length - 1];

		cfg.command = command;
		cfg.save("%s/dimcha/settings.cfg".format(getConfigPath()));
	}

	if (ARGTIME)
	{
		cfg.rawTime = VALTIME[0];
		writeln("Dimcha: Set time to ", cfg.rawTime);
		cfg.save("%s/dimcha/settings.cfg".format(getConfigPath()));
		try
			cfg.update();
		catch (ConfigUpdateError e)
		{
			writeln(e.msg);
			return -2;
		}
	}

	if (ARGCOMPOSE)
	{
		ILA newILA = VALCOMPOSE[0].composeToILA();

		if (!ARGSILENT) writeln("Dimcha: Created new ILA file for directory '%s' at '%s'!".format(VALCOMPOSE[0], VALCOMPOSE[1]));
		newILA.save(VALCOMPOSE[1]);
	}

	// Validate the config and ensure an ILA was properly configured
	try
	{
		ila.load(cfg.defaultList);
	}
	catch (ILALoadError e)
	{
		if (!ARGSILENT)
		{
			if (cfg.defaultList == "") writeln("Please specify an ILA file to use! See 'dimcha --help' for more information D:");
			else writeln("Invalid ILA file specified: %s".format(cfg.defaultList));
		}
		return -2;
	}

	if (cfg.command == "")
	{
		if (!ARGSILENT) writeln("Please specify a command to call for setting the background image! D:");
		return -3;
	}

	if (!breakLoop && !ARGSILENT) writeln("Dimcha is online ^_^.");

	while (!breakLoop)
	{
		string path = ila.select();
		if (!ARGSILENT) writeln("Dimcha: selected '%s'".format(path));

		string[] command = cfg.command.split();
		foreach (int i, part; command)
		{
			if (part == "%s")
			{
				command[i] = path;
				break;
			}
		}

		auto comp = execute(command);

		if (!ARGSILENT)
		{
			if (comp.status != 0)
				writeln("Failed to set background image!");
		}
		debug writeln(command);

		Thread.getThis().sleep(cfg.time);
	}

	return 0;
}

