module dimcha.config;

import std.stdio: File;
import std.file: exists, isFile;
import std.string: chomp, isNumeric;
import std.conv: to;

import core.time: Duration, minutes, seconds, days, hours;

import dimcha.util;

/**
	The config format is a simple one-value-per-line style file.
	First line is a version number.
	Second line is the ila file to load.
	Third line is the interval to delay.
	Fourth line is the command to execute.
**/

const string VERSION = "0.5.0";

class ConfigLoadError: Exception
{
	this(string path)
	{
		import std.stdio: writeln;
		import std.string: format;
		writeln("Cannot load config file '%s'".format(path));
		super("ConfigLoadError");
	}
}

class ConfigParseError: Exception
{
	this(string path, string additionalInformation)
	{
		import std.stdio: writeln;
		import std.string: format;
		writeln("Error parsing config file '%s'\n%s".format(path, additionalInformation));
		super("ConfigParseError");
	}
}

@safe pure nothrow class ConfigUpdateError: Exception
{
	this(string message)
	{
		super(message);
	}
}

struct ConfigFile
{
	string versionSpec;
	string defaultList;
	Duration time;
	string rawTime;
	string command;

	/// Initialize with default values.
	void loadDefault()
	{
		versionSpec = VERSION;
		defaultList = "";
		time = 1.minutes;
		rawTime = "1m";
		command = "feh --bg-max %s";
	}

	/// Load a config from a given fileName.
	void load(string fileName)
	{
		if (!exists(fileName) || !isFile(fileName))
			throw new ConfigLoadError(fileName);

		File fp = File(fileName, "r");

		versionSpec = fp.readln().chomp();
		defaultList = fp.readln().chomp();

		rawTime = fp.readln().chomp();
		if (rawTime.isNumeric)
		{
			uint value = to!uint(rawTime);
			time = value.seconds;
		} else
		if (rawTime[$-1] == 'm')
		{
			uint value = to!uint(rawTime[0..$-1]);
			time = value.minutes;
		} else
		if (rawTime[$-1] == 'h')
		{
			uint value = to!uint(rawTime[0..$-1]);
			time = value.hours;
		} else
		if (rawTime[$-1] == 'd')
		{
			uint value = to!uint(rawTime[0..$-1]);
			time = value.days;
		}
		else
		{
			throw new ConfigParseError(fileName, "Invalid duration value");
		}

		command = fp.readln().chomp();

		fp.close();
	}

	/// Update any values that need further processing, such as the Interval.
	void update()
	{
		if (rawTime.isNumeric)
		{
			uint value = to!uint(rawTime);
			time = value.seconds;
		} else
		if (rawTime[$-1] == 'm')
		{
			uint value = to!uint(rawTime[0..$-1]);
			time = value.minutes;
		} else
		if (rawTime[$-1] == 'h')
		{
			uint value = to!uint(rawTime[0..$-1]);
			time = value.hours;
		} else
		if (rawTime[$-1] == 'd')
		{
			uint value = to!uint(rawTime[0..$-1]);
			time = value.days;
		}
		else
		{
			throw new ConfigUpdateError("Invalid time specified");
		}
	}

	/// Save the config to a given fileName.
	void save(string fileName)
	{
		File fp = File(fileName, "w");

		fp.writeln(versionSpec);
		fp.writeln(defaultList);
		fp.writeln(rawTime);
		fp.writeln(command);

		fp.close();
	}
}

