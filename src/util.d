module dimcha.util;


const string HELP =
"Usage: dimcha [OPTIONS]

Options:
-l, --list               specify a list to use for the cycle.
-c, --compose            construct a new list given a target directory and a name.
-t, --time               change the length of the interval between cycles.
-k, --kill               execute all operations and shutdown after completion.
-x, --default-command    command to call for displaying the target image as background.
-s, --silent             run normally, but do not print out information.

Note:
When specifying the default command, all following args are ignored, so if you wish to
call it, ensure it is the last argument provided. Also note that the %s is where the
file name will be inserted into the command.

Examples:

Construct a new ila called default.ila in the current directory.
dimcha -c ~/docs/bgimg/ default.ila

Change the default time interval to 5 minutes per cycle.
dimcha -t 5m

Change the default time interval to 30 seconds per cycle.
dimcha -t 30

Change the default command called to set background.
dimcha -x feh --bg-max %s";

@safe pure nothrow class InitializationFailure: Exception
{
	this(string message)
	{
		super(message);
	}
}

/// Builds the command string.
string buildCommand(string fmt, string filePath)
{
	import std.string: format;
	return fmt.format(filePath);
}

/// Fetches the proper location to store data.
string getDataPath()
{
	import std.process: environment;
	import std.string: format;

	string basePath;
	debug
	{
		basePath = "testcfg";
	}
	else
	{
		version(Windows) basePath = environment["%APPDATA%"];
		version(linux)
		{
			string dataDir;
			if ((dataDir = environment.get("XDG_DATA_DIRS")) !is null)
			{
				import std.string: split;
				basePath = dataDir.split(":")[0];
			}
			else
			{
				import std.string: format;
				basePath = environment.get("XDG_DATA_HOME", "%s/.local/share".format(environment.get("HOME")));
			}
		}
	}
	return basePath;
}

/// Fetches the proper location to store configuration.
string getConfigPath()
{
	import std.process: environment;
	import std.string: format;
	string basePath;
	debug
	{
		basePath = "testcfg";
	}
	else
	{
		version(Windows) basePath = environment["%APPDATA%"];
		version(linux)
		{
			string dataDir;
			if ((dataDir = environment.get("XDG_CONFIG_DIRS")) !is null)
			{
				import std.string: split;
				basePath = dataDir.split(":")[0];
			}
			else
			{
				import std.string: format;
				basePath = environment.get("XDG_CONFIG_HOME", "%s/.config".format(environment.get("HOME")));
			}
		}
	}
	return basePath;
}

/// Determines if this is the first time dimcha has been run or not by testing if previous configuration exists.
bool firstRun()
{
	import std.string: format;
	import std.file: exists, isFile;

	string basePath = "%s/dimcha".format(getConfigPath());

	if (basePath.exists && basePath.isFile)
	{
		throw new InitializationFailure("Intialization Failure: Cannot create configuration directory");
	} else
	if (basePath.exists && !basePath.isFile)
	{
		return false;
	}
	else
	{
		import std.file: mkdirRecurse;
		mkdirRecurse("%s/".format(basePath));
		mkdirRecurse("%s/dimcha".format(getDataPath()));
		return true;
	}
}

/// Converts a flat filename to a full path or just returns an actual path.
string generatePath(string path, string expandedPath)
{
	import std.path: pathSplitter;
	import std.array: array; // needed to work around a stupidry in pathsplitters
	import std.string: format;

	if (path.pathSplitter().array.length == 1)
		return "%s/dimcha/%s".format(expandedPath, path);
	else
		return path;
}

