# HOW DO
Compilation is performed with the dub build tool, which can be found
[here](code.dlang.org). Alternatively, there is a simplistic makefile.

You **must** have `dmd` installed to use either of these methods without
modification. I wrote dimcha using `DMD64 D Compiler v2.070-devel-e90436f`
and `DUB version 0.9.24+182-g7b9eba4, built on Apr  1 2016`, but afaik the
last stable builds ought to work. If not, this is a bug that needs to be
fixed :D.

The makefile supports DESTDIR, so package creation ought to be relatively simple.

You can find `dmd` [here](dlang.org).

## Dub
execute `dub build -b release`
execute `sudo make install`

## Makefile
execute `make`
execute `sudo make install`

# Manpages
Dimcha includes a manpage, but to compile it you need `ronn`.
The easiest way to aquire `ronn` is to use ruby gem.

`sudo gem install ronn` should pull it in for you.

Alternatively, all relevant info can be found via `dimcha --help`.

After this just execute `make docs` and `make docs-install`.

