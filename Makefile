DC = dmd
BIN = dimcha
DFLAGS = -Isrc

.PHONY: all install uninstall clean docs docs-install

all: $(BIN)

$(BIN):
	$(DC) -release -of$@ src/main.d src/ila.d src/util.d src/config.d src/argparse.d
	rm $(BIN).o

install: $(BIN)
	@mkdir -p $(DESTDIR)/usr/local/bin
	@cp $(BIN) $(DESTDIR)/usr/local/bin/$(BIN)
	@echo "Installed $(BIN) to $(DESTDIR)/usr/local/bin"

uninstall:
	@$(RM) $(DESTDIR)/usr/local/bin/$(BIN)
	@echo "Deleted $(BIN)"

clean:
	@-$(RM) $(BIN) dub.selections.json __test__library__ doc/dimcha.1.gz doc/dimcha.1

docs:
	ronn --manual=dimcha --organization=dimcha -roff doc/dimcha.1.ronn
	gzip -f doc/dimcha.1

docs-install:
	@mkdir -p $(DESTDIR)/usr/share/man/man1
	@cp doc/dimcha.1.gz $(DESTDIR)/usr/share/man/man1/.
	@echo "Installed manpages to $(DESTDIR)/usr/share/man/man1"
